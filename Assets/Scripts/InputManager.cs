﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    private PlayerController playercontroller;
    private float sensitivity = 3.0f;
    private LookRotation lookRotation;
    private MouseCursor mouseCursor;

    private Laser laser;
   private BallShot ballShoot;


    void Start()
    {
        playercontroller = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        lookRotation = playercontroller.GetComponent<LookRotation>();
        laser = playercontroller.GetComponent<Laser>();
        ballShoot = playercontroller.GetComponent<BallShot>();

        mouseCursor = new MouseCursor();
        mouseCursor.HideCursor();
    }
    void Update()
    {
        //El movimiento del player
        Vector2 inputAxis = Vector2.zero;
        inputAxis.x = Input.GetAxis("Horizontal");
        inputAxis.y = Input.GetAxis("Vertical");
        playercontroller.SetAxis(inputAxis);
        //El salto del player
        if (Input.GetButton("Jump")) playercontroller.StartJump();

        //Rotación de la cámara
        Vector2 mouseAxis = Vector2.zero;
        mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
        mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;
        //Debug.Log("Mouse X = " + mouseAxis.x + " Y = " + mouseAxis.y);
        lookRotation.SetRotation(mouseAxis);

        //Cursor del ratón
        if (Input.GetMouseButtonDown(0)) mouseCursor.HideCursor();
        else if (Input.GetKeyDown(KeyCode.Escape)) mouseCursor.ShowCursor();

        if (Input.GetMouseButtonDown(0)) laser.Shot();
        if (Input.GetKeyDown(KeyCode.R)) laser.Reload();

        //if(Input.GetMouseButtonDown(1)) ballShoot.Shot();

    }
}